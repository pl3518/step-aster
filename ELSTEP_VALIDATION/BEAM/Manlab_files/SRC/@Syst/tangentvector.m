function [Ut_tot,Vt_tot,Jacobian] = tangentvector(obj,Jacobian)
% Computation of the tangent vector at U0 using the Jacobian matrix dRdU at U0
% dRdU is augmented with a random vector

% We assume that lambda is not a limit-point. In theory there could be
% issues but the ending point of a continuation branch is never close
% enough to a limit-point

% It returns the tangent vector before normalization : to
% improve efficiency of obj.ANMSeries.

%% Test if there are auxiliar variables.
if ~isempty(Jacobian.dRauxdUaux)
    [Jacobian.LK_aux,Jacobian.UK_aux] = lu(Jacobian.dRauxdUaux,'vector');
    Jacobian.Kfull = Jacobian.dRdU - Jacobian.dRdUaux*(Jacobian.UK_aux\(Jacobian.LK_aux\Jacobian.dRauxdU));
else
    Jacobian.Kfull = Jacobian.dRdU;
end

%% Default column we put in the right-hand-side
Jacobian.missingcolumn = Jacobian.Kfull(:,end);
Jacobian.K = Jacobian.Kfull(:,1:end-1);

% Fast lu factorization
[Jacobian.LK,Jacobian.UK,Jacobian.pK,Jacobian.qK] = lu(Jacobian.K,'vector');

%% To avoid issues with vertical branches
% The column sent to the right-hand-side changes if
% an eigenvalue of K is smaller than tol.

[val,i] = min(abs(diag(Jacobian.UK)));

tol = 1e-12;

if val < tol
    % The column corresponding to the smallest eigenvalue is sent to the
    % right-hand side.
    Jacobian.index_exchange = Jacobian.qK(i);
    Jacobian.list_val = [1:Jacobian.index_exchange-1 Jacobian.index_exchange+1:obj.neq+1];
    Jacobian.missingcolumn = Jacobian.Kfull(:,Jacobian.index_exchange);
    Jacobian.K = Jacobian.Kfull(:,Jacobian.list_val);
    
    % Fast lu factorization
    [Jacobian.LK,Jacobian.UK,Jacobian.pK,Jacobian.qK] = lu(Jacobian.K,'vector');
    
    Jacobian.vertical_tangent = 1;
else
    Jacobian.index_exchange = obj.neq+1;
    Jacobian.list_val = 1:obj.neq;
    
    Jacobian.vertical_tangent = 0;
end

[Vt,Vt_aux] = obj.Condensation(Jacobian,sparse(obj.neq,1),sparse(obj.neq_aux,1),1);
Vt_tot = [Vt;Vt_aux];

Ut_tot = Vt_tot *  1/sqrt((obj.arclengthdef.*Vt_tot)'*Vt_tot);

end