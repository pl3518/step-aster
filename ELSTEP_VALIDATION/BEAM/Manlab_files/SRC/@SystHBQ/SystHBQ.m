classdef SystHBQ < Syst
    
    properties
        
        H;  % Order of truncation of Fourier series
        
        subtype = 'autonomous' ; % subtype of system: {['autonomous'],'forced'}
        
        nz_tot ;    % number of equation of the DAE
        nz ;        % number of main equations
        nz_aux=0;   % number of auxiliary equations
        
        pulsation='omega';  % for forced system, the continuation parameter is omega by default. 
                        % Otherwise, sys.pulsation is the value of the
                        % pulsation of the forcing terms
        
        % Small operator on the Algebro-differential equations
        id =[]; jd =[]; vd= [];                % list for the sparse (order 2) tensor d
        id1=[]; jd1=[]; vd1=[];                % list for the sparse (order 2) tensor d1
        idd=[]; jdd=[]; vdd=[];                % list for the sparse (order 2) tensor dd
        ic0=[]; vc0=[];                        % list for the sparse (order 1) tensor c0
        ic1=[]; vc1=[];                        % list for the sparse (order 1) tensor c1
        ic2=[]; vc2=[];                        % list for the sparse (order 1) tensor c1
        il0=[]; jl0=[]; vl0=[];                % list for the sparse (order 2) tensor l0
        il1=[]; jl1=[]; vl1=[];                % list for the sparse (order 2) tensor l1
        iq =[]; jq =[]; kq =[]; vq=[];         % list for the sparse (order 3) tensor q
        
        iforce0=[]; hforce0=[]; vforce0=[];       % list for the sparse (order 2) tensor force
        iforce1=[]; hforce1=[]; vforce1=[];       % list for the sparse (order 2) tensor force
        
        idl=[]; jdl=[]; vdl=[];                % list for the sparse (order 2) tensor dl
        idq=[]; jdq=[]; kdq=[]; vdq=[];        % list for the sparse (order 3) tensor dq
                
        zi_phase=1; % index of the state variable reciving the phase condition zi'(0)=0
        % set to first one by default
    end
    
    methods
        
        function sys = SystHBQ(nz,nz_aux,H,equations,point_display,global_display,parameters,subtype,pulsation)
            
            sys=sys@Syst('neq',nz*(2*H+1)+1,'neq_aux',nz_aux*(2*H+1)+2);
            
            sys.type = 'HBQ';
            
            sys.parameters = parameters;
            sys.equations = equations;
            sys.point_display = point_display;
            sys.global_display = global_display;
            
            sys.nz = nz;
            sys.nz_aux = nz_aux;
            sys.nz_tot = nz + nz_aux;
            sys.H  = H ;    % harmonic number
            
            % Arclength only on the main variables
            sys.arclengthdef = sparse(1:sys.neq+1,ones(sys.neq+1,1),1,sys.ninc,1);
            
            % Creation of the little operators
            sys = sys.get_operators;
            
            % subtype
            if nargin > 7
            sys.subtype = subtype;
            end
            
            % Pulsation of the oscillations fixed to a constant
            if nargin == 9
                sys.pulsation = pulsation;
            end
            
            % Residu
            sys.R = @R;
            
            function [Rtot] = R(obj,Utot)
                %  Compute  R(U)
                H    = obj.H;   % Harmonic number
                nz_tot   = obj.nz_tot;  % number of equation of the DAE system
                DHp1 = 2*H+1 ;
                
                % Extract informations from Utot
                [Ztot,omega,lambda,omega2,lambdaomega] = obj.get_Ztot(Utot);
                Rfourier=zeros(DHp1,nz_tot);
                
                %   R = C0 + lambda C1  + L0(U) + lambda L1(U) +
                %       omega D(U) + lambdaomega D1(U) + omega2 DD(U) + Q(U,U)
                %
                %   R is formed by block using the list ic0, vc0, ......
                for i=1:size(obj.iforce0,1)
                    Rfourier(obj.hforce0(i),obj.iforce0(i)) = Rfourier(obj.hforce0(i),obj.iforce0(i)) - obj.vforce0(i);
                end
                
                for i=1:size(obj.iforce1,1)
                    Rfourier(obj.hforce1(i),obj.iforce1(i)) = Rfourier(obj.hforce1(i),obj.iforce1(i)) - lambda*obj.vforce1(i);
                end
                
                for i=1:size(obj.ic0,1)
                    Rfourier(1,obj.ic0(i))= Rfourier(1,obj.ic0(i)) + obj.vc0(i) ;
                end
                
                for i=1:size(obj.ic1,1)
                    Rfourier(1,obj.ic1(i))= Rfourier(1,obj.ic1(i)) + obj.vc1(i)*lambda ;
                end
                
                for i=1:size(obj.ic2,1)
                    Rfourier(1,obj.ic2(i))= Rfourier(1,obj.ic2(i)) + obj.vc2(i)*lambda^2 ;
                end
                
                for i=1:size(obj.il0,1)
                    Rfourier(:,obj.il0(i))= Rfourier(:,obj.il0(i)) + obj.vl0(i)*Ztot(:,obj.jl0(i)) ;
                end
                
                for i=1:size(obj.il1,1)
                    Rfourier(:,obj.il1(i))= Rfourier(:,obj.il1(i)) + (obj.vl1(i)*lambda)*Ztot(:,obj.jl1(i)) ;
                end
                
                for i=1:size(obj.id,1)
                    Rfourier(:,obj.id(i))= Rfourier(:,obj.id(i)) + (obj.vd(i)*omega)*obj.D(Ztot(:,obj.jd(i))) ;
                end
                
                for i=1:size(obj.id1,1)
                    Rfourier(:,obj.id1(i))= Rfourier(:,obj.id1(i)) + (obj.vd1(i)*lambdaomega)*obj.D(Ztot(:,obj.jd1(i))) ;
                end
                
                for i=1:size(obj.idd,1)
                    Rfourier(:,obj.idd(i))= Rfourier(:,obj.idd(i)) + (obj.vdd(i)*omega2)*obj.D(obj.D(Ztot(:,obj.jdd(i)))) ;
                end
                
                for i=1:size(obj.iq,1)
                    Rfourier(:,obj.iq(i))= Rfourier(:,obj.iq(i)) + obj.vq(i)*( obj.Prod(Ztot(:,obj.jq(i)),Ztot(:,obj.kq(i))) ) ;
                end
                
                
                % phase equation
                switch obj.subtype
                    case 'autonomous'
                        Rphase= (1:H)*Ztot(H+2:2*H+1,obj.zi_phase);
                    case 'forced'
                        if isfloat(obj.pulsation)
                            Rphase = omega - obj.pulsation;
                        else
                            Rphase = omega - lambda;
                        end
                end
                
                Romega2 = omega2 - omega*omega;
                Rlambdaomega = lambdaomega - lambda*omega;
                
                if ~isempty(obj.idl)
                    for i=1:size(obj.idl,1)
                        Rfourier(:,obj.idl(i))= Rfourier(:,obj.idl(i)) + obj.vdl(i)*obj.D(Ztot(:,obj.jdl(i))) ;
                    end
                    
                    for i=1:size(obj.idq,1)
                        Rfourier(:,obj.idq(i))= Rfourier(:,obj.idq(i)) + obj.vdq(i)*( obj.Prod(Ztot(:,obj.jdq(i)),obj.D(Ztot(:,obj.kdq(i)))) ) ;
                    end
                    
                    Ztot_t0 = sum(Ztot(1:H+1,:),1);
                    cond_ini = obj.equations(obj,0,[Ztot_t0 lambda]',zeros(obj.nz_tot,1),zeros(obj.nz_tot,1));
                    Rfourier(1,obj.idl) = cond_ini(obj.idl);
                end
                
                % We divide Rfourier between the main equations and the auxiliary
                % equations. We add the phase equation and the definition of omega^2 and lambda*omega
                Rfourier = reshape(Rfourier,nz_tot*DHp1,1);
                R = [Rfourier(1:obj.neq-1); Rphase];
    
                Raux = [Rfourier(obj.neq:end);Romega2;Rlambdaomega];
                Rtot = [R;Raux];
                
            end
        end
        %% All the function used to compute the residu and the jacobian from the little operators
        function [Ztot,omega,lambda,varargout] = get_Ztot(obj,Utot)
            
            DHp1 = obj.H*2+1;
            Z     =reshape(Utot(1:obj.neq-1),DHp1,obj.nz);
            omega =Utot(obj.neq);
            lambda=Utot(obj.neq+1) ;
            
            varargout{1} = Utot(end-1); % omega2 =Utot(end-1);
            varargout{2} = Utot(end);   % lambdaomega =Utot(end);
            Zaux = reshape(Utot(obj.neq+2:end-2),DHp1,obj.nz_aux);
            
            Ztot = [Z Zaux];
        end
        
        function  DU = D(obj,U)
            %  Compute  D.U
            Hu=(size(U,1)-1)/2   ;   % harmonic number
            DU= [ zeros(1,size(U,2)) ; (1:Hu)'.*U(Hu+2:end,:) ; (-(1:Hu))'.*U(2:Hu+1,:) ];
        end
        
        function R = Prod(obj,U,V)
            % Compute the product of U and V using complex convolution
            Hu = (size(U,1)-1)/2;
            Hv = (size(V,1)-1)/2;
            
            if Hv > Hu
                Utemp = U;U=V;V=Utemp;
            end
            
            U = full(U);
            V = full(V);
            
            Ucompl = obj.real_to_compl(U);
            Vcompl = obj.real_to_compl(V);
            Ucompl_tot = [conj(Ucompl(end:-1:2,:));Ucompl];
            Vcompl_tot = [conj(Vcompl(end:-1:2,:));Vcompl];
            Rcompl_tot = zeros(size(U));
            for i=1:size(U,2)
                Rcompl_tot(:,i) = conv(Ucompl_tot(:,i),Vcompl_tot(:,i),'same');
            end
            Rcompl = Rcompl_tot(obj.H+1:end,:);
            R = obj.compl_to_real(Rcompl);
        end
        
        function Bmat = B(obj,U)
            %  Compute  the matrix B from U (size 2H+1 )
            %
            %  toeplitz(c,r) generate a matrix from a colum c and a row r
            %
            Hu=(size(U,1)-1)/2   ;   % harmonic number
            Uc= U(2:Hu+1)/2;           % Half of the cosines
            Us= U(Hu+2:2*Hu+1)/2;       % Half of the sines
            
            LUc= [ U(1) ; Uc(1:end-1) ];    %   [ U0 ; Uc1/2 ; Uc2/2 ...]
            LUs= [   0  ; Us(1:end-1) ];    %   [ 0  ; Us1/2 ; Us2/2 ...]
            CC = toeplitz(LUc,LUc);
            SS = toeplitz(-LUs,LUs);
            
            Cp = zeros(Hu,Hu) ;
            Sp = zeros(Hu,Hu) ;
            L0 = zeros(Hu,1) ;
            VUc= [   0  ; Uc(end:-1:2) ];     %   [ 0  ; UcH/2 ; UcH-1/2 ...]
            VUs= [   0  ; Us(end:-1:2) ];     %   [ 0  ; UsH/2 ; UsH-1/2 ...]
            Cp(:,Hu:-1:1) = toeplitz(L0,VUc);
            Sp(:,Hu:-1:1) = toeplitz(L0,VUs);
            
            Bmat(:,1)= U ;
            
            Bmat(1,2:Hu+1) = Uc ;
            Bmat(1,Hu+2:2*Hu+1) = Us ;
            
            Bmat(2:Hu+1    ,2:Hu+1    )=CC + Cp;
            Bmat(2:Hu+1    ,Hu+2:2*Hu+1)=SS + Sp;
            Bmat(Hu+2:2*Hu+1,2:Hu+1    )=SS'+ Sp;
            Bmat(Hu+2:2*Hu+1,Hu+2:2*Hu+1)=CC - Cp;
        end
        
        function Ureal = compl_to_real(obj,Ucompl)
            % function Ureal = compl_to_real(sys,Ucompl)
            % Take a vector of complex harmonics  from 0 to H and give
            % the real corresponding Fourier series (constant, cos, sin)
            
            Ucos = 2*real(Ucompl(2:end,:));
            Usin = -2*imag(Ucompl(2:end,:));
            Ureal = real([Ucompl(1,:); Ucos ; Usin]);
        end
        
        function Ucompl = real_to_compl(obj,Ureal)
            % function Ucompl = real_to_compl(sys,Ureal)
            % It is the inverse function of compl_to_real
            Hu=(size(Ureal,1)-1)/2   ;   % harmonic number
            Ucompl = [Ureal(1,:);0.5*Ureal(2:Hu+1,:)-0.5*1i*Ureal(Hu+2:end,:)];
        end
        
    end
end


