function [new_nodes]=evaluate_index_position_after_clamping(clamped_nodes,nodes)
    %clamped_nodes=sort(clamped_nodes);
    nodes=sort(nodes);
    
    all_nodes=sort([clamped_nodes;nodes]);
    
    new_nodes=zeros(size(nodes));
    
    
    for i=1:length(new_nodes)
        new_nodes(i)=nodes(i)-(find(all_nodes==nodes(i))-i);
    end
    
    
end