function [Jacobian] = Jacobian(obj,Utot)
% Compute the Jacobian of R with respect to Utot
%   dRtotdUtot =  L(.)   + 2*Q(U, . )  + dL(.) + dQ(U,.)
% It gives the block decomposition of the jacobian matrix in the field of
% the structure "Jacobian".

dRtotdUtot = sparse(obj.iL,obj.jL,obj.vL,obj.neq_tot,obj.ninc) ...
    + sparse(obj.iQ,obj.kQ,2*obj.vQ.*Utot(obj.jQ),obj.neq_tot,obj.ninc) ...
    + sparse(obj.idL,obj.jdL,obj.vdL,obj.neq_tot,obj.ninc) ...
    + sparse(obj.idQ,obj.kdQ,obj.vdQ.*Utot(obj.jdQ),obj.neq_tot,obj.ninc) ;

    Jacobian.dRtotdUtot = dRtotdUtot;
    Jacobian.dRdU = dRtotdUtot(1:obj.neq,1:obj.neq+1);
    Jacobian.dRdUaux = dRtotdUtot(1:obj.neq,obj.neq+2:end);
    Jacobian.dRauxdU = dRtotdUtot(obj.neq+1:end,1:obj.neq+1);
    Jacobian.dRauxdUaux = dRtotdUtot(obj.neq+1:end,obj.neq+2:end);

end


