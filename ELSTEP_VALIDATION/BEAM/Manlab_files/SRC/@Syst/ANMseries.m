function [U0_tot,Amax, BifData, StabData] = ANMseries(obj,U0tot,Ut0)
% Compute ANM series for sys(U0val)
% Outputs: U0_tot (taylor type), Amax (domaine of utility),
% BifData is a structure variable used to store bifurcation point data
% when one is found  properties : Ubif,Utf, Utb,alpha,status
% StabData is a structure variable used to store stability data
% when a change of stability is found  properties : Uchange, Achange, status

global Ck

%% Tangent vector - branch orientation

Jacobian = obj.Jacobian(U0tot);% Block decomposition of the Jacobian at point U0tot

[U1,Vt_tot,Jacobian] = obj.tangentvector(Jacobian);     % tangent vector at U0tot

if obj.StabilityCheck == 1; [stab_Uinit,nb_realpos] = obj.Stability(U0tot,Jacobian); end

if (U1'*Ut0) < 0, U1 = - U1; end  % set U1 in the direction of Ut0

%% series computation : Zero and First order
U0_tot=Taylor(get(obj,'order'),U0tot);
U0_tot=set(U0_tot,'coef1',U1);

%% series computation : Orders p>=2
Rhs0_tot = U1/(U1'*Vt_tot);

for p=2:obj.order
    Ck=p;
    
    % Fast computation using the condensation function :
    Fpnl_tot = obj.Fpnl(U0_tot,p);
    
    [Rhs1,Rhs1_aux] = Condensation(obj,Jacobian,-Fpnl_tot(1:obj.neq),-Fpnl_tot(obj.neq+1:end),0);
    Rhs1_tot = [Rhs1;Rhs1_aux];
    
    Usol_missing = - Rhs0_tot'*Rhs1_tot;
    Usol_tot = Rhs1_tot + Usol_missing*Vt_tot;
    
    U0_tot=set(U0_tot,'coefk',Usol_tot,p);
end

%% Detection of emerging geometric series

[Uclean, BifData.Uscale, BifData.alpha, BifData.status] = obj.GeomSerie(U0_tot);

switch BifData.status
    case 'simplebif'
        U0_tot=Uclean;
        Fpnl_tot=obj.Fpnl(U0_tot,p);
    case 'nothing'
        
end

%% domaine of utility Amax
if (norm(Fpnl_tot)==0)
    Amax = obj.Amax_max;
    disp(['ANMSeries:Infinite radius, Amax set to ' num2str(Amax)]);
  else
    Amax = (obj.ANMthreshold/norm(Fpnl_tot))^(1/(obj.order));
    Amax = min(Amax,obj.Amax_max);
end

%% Extraction of emerging geometric series

if obj.BifDetection == 1
    switch BifData.status
        case 'nothing'
            
        case 'simplebif'
            [BifData.Ubif, BifData.Utf, BifData.Utb]=obj.locatebif(Uclean, BifData.Uscale, BifData.alpha);
    end
end

%% Stability of the solution
if obj.StabilityCheck == 1
    [StabData.Uchange,StabData.Achange,StabData.status,StabData.eigen] = obj.StabilityComputation(U0_tot,Amax,stab_Uinit,nb_realpos);
else
    StabData.Uchange = 0;
    StabData.Achange = 0;
    StabData.eigen.type = 'nothing';
    StabData.status = {'stable','stable'};
end

end

