function [] = point_display(obj,Utot)

%Reconstruction des temporels dans la matrice Ut(nt,nz)
H=obj.H;
[Ztot,omega,~]=obj.get_Ztot(Utot);
T=2*pi/omega;
temps=0:T/(20*H+1):T;
nt=length(temps);

VectCos=cos(omega*temps'*(1:H));
VectSin=sin(omega*temps'*(1:H));
MAT=[ ones(nt,1) , VectCos , VectSin ];  % MAT(nt,2*H+1)
Ut=MAT*Ztot;  % Ut(nt,nz)

% 29 nodes along the axis
%Displx=Ut(:,1:obj.nz/2)*obj.parameters.Phi(obj.parameters.axis_nodes*3-2,:)';  %Displx(nt,29)
Disply=Ut(:,1:obj.nz/2)*obj.parameters.Phi(obj.parameters.axis_nodes*3-1,:)';
Displz=Ut(:,1:obj.nz/2)*obj.parameters.Phi(obj.parameters.axis_nodes*3,:)';
% 15 is the center node, 13 is the node at .433m
center=15;
decentered=13;
tt=find(Disply(:,center)==max(Disply(:,center)));

figure(500);hold on;title('axis deformation')
subplot(2,1,1);hold on;
title('y-displacement of axis nodes at maximum displacement time instant')
stem(Disply(tt,:),'b^');

subplot(2,1,2);hold on;
title('y-displacement of axis nodes at maximum displacement time instant')
stem(Displz(tt,:),'k^')


figure(600);hold on;title('displacement over time ');
subplot(3,1,1);hold on;
title('y-displacement of center node over time ');
plot(Disply(:,center),'b');

subplot(3,1,2);hold on;
title('y-displacement of the node at z=0.433m over time ');
plot(Disply(:,decentered),'b');

subplot(3,1,3);hold on;
title('z-displacement of the node at z=0.433m over time ');
plot(Displz(:,decentered),'k');

end
