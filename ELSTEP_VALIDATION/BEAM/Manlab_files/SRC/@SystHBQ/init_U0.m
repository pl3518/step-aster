function [U0] = init_U0(obj,Z,omega,lambda)

H = obj.H;
DHp1 = 2*H+1;

if numel(Z) == DHp1*obj.nz_tot
    Ufourier = reshape(Z(:,1:obj.nz),DHp1*obj.nz,1);
    Uaux_fourier = reshape(Z(:,obj.nz+1:end),DHp1*obj.nz_aux,1);
else
    warndlg('init_U0 : Wrong size of the input Z.');
end


U0 = [Ufourier;omega;lambda;Uaux_fourier;omega^2;lambda*omega];

end

