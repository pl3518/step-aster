function [status,realpos,eig_val,eig_vec] = Stability(obj,Utot,Jacobian)

%% Linear stability
K = Jacobian.K;

[eig_vec,eig_val] = eig(full(K));
eig_val = diag(eig_val);
real_part = real(eig_val);
realpos = sum(real_part>obj.StabTol);

if realpos > 0
    status = 0;
else
    status = 1;
end

if ~isempty(Jacobian.dRauxdUaux)
    
    Kaux = Jacobian.dRauxdUaux;
    C = Jacobian.dRauxdU;
    
    %%% Without lambda
    eig_vec_aux = - Kaux\(C(:,1:end-1)*eig_vec);
    eig_vec = [eig_vec ; eig_vec_aux];
end

% figure(7)
% plot(eig_val,'o');hold on;
% line([0 0],[min(imag(eig_val)) max(imag(eig_val))],'color','k');

end


