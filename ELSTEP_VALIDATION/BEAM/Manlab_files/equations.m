function [Rtot,dRaux,Forcing] = equations(sys,t,Utot,dUtot,~)
 

%% parameters of the system
mass = sys.parameters.mass;
stiffness = sys.parameters.stiffness;
damping = sys.parameters.damping;

force = sys.parameters.force;

alpha = sys.parameters.alpha;
beta = sys.parameters.beta;

n_modes=sys.parameters.n_modes;


%% variables of the system
x = Utot(1:sys.nz/2,:);dx=dUtot(1:sys.nz/2,:);
y = Utot(sys.nz/2+1:sys.nz,:);dy=dUtot(sys.nz/2+1:sys.nz,:);
r = Utot(sys.nz+1:sys.nz+sys.nz_aux,:);
lambda = Utot(end); % unused here because it is the forcing pulsation.



%% Auxiliary equations
Raux = zeros(sys.nz_aux,1);     dRaux = zeros(sys.nz_aux,1);
% The auxiliary variable r is a vector of length  nz_aux=L*(L+1)/2  
% in the form:
%     r={x1*{x1 x2 .. xL}  x2*{x2 x3 .. xL}  ..  xL-1*{xL-1 xL}  xL{xL}}
% function for picking the p-th element of r that corresponds to x(j)*x(k)
p=@(j,k) (2*n_modes-j+2)*(j-1)/2+1-j+k;
for j=1:n_modes
    for k=j:n_modes
        Raux(p(j,k)) = r(p(j,k)) - x(j)*x(k);
    end
end


%% Physical equations
R = zeros(sys.nz,1);
R(1:sys.nz/2)= y - dx;
R(sys.nz/2+1:sys.nz)= - stiffness*x - damping*y -  mass*dy;
for j=1:n_modes
    for k=j:n_modes
        R(sys.nz/2+1:sys.nz) =R(sys.nz/2+1:sys.nz) - alpha(:,j,k)*r(p(j,k));
        for l=k:n_modes
            R(sys.nz/2+1:sys.nz) =R(sys.nz/2+1:sys.nz) - beta(:,j,k,l)*r(p(j,k))*x(l);
        end
    end
end

%% All the equations of the quadratic system
Rtot = [R;Raux];

%% Forcing terms
% Should be written as if the forcing pulsation value is 1
% i.e. the forcing period is 2*pi
Forcing = zeros(2*sys.H+1,sys.nz_tot);
for kk=sys.nz/2+1:sys.nz
    Forcing(:,kk) = force(kk-sys.nz/2)*cos(t);
end

end
