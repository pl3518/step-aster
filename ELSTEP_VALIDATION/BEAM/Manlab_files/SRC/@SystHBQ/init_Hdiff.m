function [Unew] = init_Hdiff(obj,Uold)

Hold = ((size(Uold,1)-4)/obj.nz_tot-1)/2;

neq_old = obj.nz*(2*Hold+1);
neq_aux_old = obj.nz_aux*(2*Hold+1);
Zold = reshape(Uold([1:neq_old neq_old+3:neq_old+3+neq_aux_old-1]) , 2*Hold+1 , obj.nz_tot); 
omega = Uold(neq_old+1);
lambda = Uold(neq_old+2);

Hnew = obj.H;
DHp1 = 2*Hnew+1;

if Hold >= Hnew
    Znew = Zold([(1:Hnew+1) (Hold+2:Hold+1+Hnew)],:);
else
    Znew = zeros(DHp1,obj.nz_tot);
    Znew([(1:Hold+1) (Hnew+2:Hnew+1+Hold)],:) = Zold;
end

Unew = obj.init_U0(Znew,omega,lambda);

end

