function [parameters]=import_results_from_code_aster()
%
%
% this function imports results from code aster such as eigenvectors of the
% selected modes, natural frequencies of the system, nonlinear stiffness
% tensors, etc. 
% and generates stiffness, mass, and damping matrices in modal coordinates
% as well as the force vector (200N middle node) in modal coordinates
%
%
%
%
%% Import coordinates of clamped nodes
clamped_nodes_temp=importdata('../nodes/clamped_nodes.txt');
clamped_nodes=clamped_nodes_temp(:,1);
clamped_dofs=zeros(3*length(clamped_nodes),1);
clamped_dofs(1:3:end)=(clamped_nodes-1)*3+1;
clamped_dofs(2:3:end)=(clamped_nodes-1)*3+2;
clamped_dofs(3:3:end)=(clamped_nodes-1)*3+3;


%% Build vector of axis coordinates (for plotting deformations afterwards)
Nodes_coordinates=importdata('../nodes/nodes_coordinates.txt');
mask = Nodes_coordinates(:,2)==0 & Nodes_coordinates(:,3)==0 & Nodes_coordinates(:,4)>0 & Nodes_coordinates(:,4)<1;
axis_nodes=Nodes_coordinates(mask,1);
Axis_coord=Nodes_coordinates(mask,:);
Axis_coord=sortrows(Axis_coord,1);
axis_nodes=evaluate_index_position_after_clamping(clamped_nodes,axis_nodes);
Axis_coord(:,1)=axis_nodes;
Axis_coord=sortrows(Axis_coord,4);
axis_nodes=Axis_coord(:,1);

%% Find the central node where the force is applied
center_node=axis_nodes(Axis_coord(:,4)==.5);


%% Import eigenvectors matrix
load '../nonlinear_tensor/modes.mat' ch2;
Phi=ch2;
% remove the zero-displacement clamped nodes from the modes
Phi(clamped_dofs,:)=[];


%% Select modes of interest: first bending mode and fourth longitudinal mode
selected_modes=[1,34];
n_modes=length(selected_modes);


%% Import eigenfrequencies
frequencies_temp=importdata('../nonlinear_tensor/freq.txt');
frequencies=frequencies_temp(selected_modes,2);


%% Build reduced matrices in modal coordinates
% Modal stiffness
Kr=diag((frequencies*2*pi).^2);
% Modal mass
Mr=eye(n_modes);
% Damping
Cr=3*Mr;


%% Import nonlinear stiffness tensors
load '../nonlinear_tensor/nonlinear_tensor.mat' alpha beta;
alpha=alpha(selected_modes,:,:);
beta=beta(selected_modes,:,:,:);


%% Build modal force vector
F0= zeros(length(Phi),1);% in real coordinates
F0(3*center_node-1)=-2e2; % 200N in the central node along y-direction
Fr=Phi'*F0; % in modal coordinates


%% save parameters for Manlab analysis
parameters.selected_modes=selected_modes;
parameters.n_modes = n_modes;

parameters.frequencies=frequencies;
parameters.Phi=Phi;

parameters.stiffness=Kr;
parameters.mass=Mr;
parameters.damping =Cr;

parameters.force =Fr;

parameters.alpha=alpha;
parameters.beta=beta;

parameters.axis_nodes=axis_nodes;

end
