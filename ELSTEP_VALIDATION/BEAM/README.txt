#######################
## README_CODE_ASTER ##
#######################

3D_BEAM MODEL (ELSTEP.comm):

L = 1 m; S = 0.3 × 0.3 m^2 ; E = 210 GPa; ν = 0.3 ; ρ = 7800 kg/m^3;

HEXA20 (quadratic element) → 471 total nodes (nodes_coordinates.txt) - 39 clamped (clamped_nodes.txt) = 432 active nodes;
→ 15 elements along the length; 
→ 2x2 elements on the cross section;

The quadratic and cubic stiffness nonlinear tensors have been calculated according to the NASA's paper (NASA_paper.pdf) and saved in  "nonlinear_tensor/nonlinear_tensor.mat" → ELSTEP 

To evaluate the nonlinear quadratic and cubic stiffness coefficients an algebraic system has been solved; in "READ" folder is possible to find the example of second loop. (Algebraic_system.pdf)

Mesh_MED3.0.med --> compatible with SALOME_2017
Mesh_MED3.3.med --> compatible with SALOME_2018

################################

The file "matrices.comm" has only been used to extract the mass and linear stiffness matrices of the system and save them in "matrices/mass.mat", "matrices/stiffness.mat".  These matrices althought, have not being used in Manlab where the mass and linear stiffness matrices are directly computed in modal coordinates.



#######################
#### README_MANLAB ####
#######################

In the folder 'Manlab_files' the file main.m launches Manlab 
and solves the forced response of the 3D beam model when a periodic force
F = -200 N along y-direction 
is applied at the center node of the beam of coordinates -> x = 0, y = 0, z = 0.5

The selected modes taken into account in the analysis are only the first bending (mode 1) and the fourth axial (mode 34).

The system of equations is discretised in time using 3 harmonics in the HBM and traced along the forcing frequency parameter.





