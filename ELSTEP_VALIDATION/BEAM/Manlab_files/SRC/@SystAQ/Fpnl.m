function [Fpnl_tot] = Fpnl(obj,U,p)
% Compute  the r.h.s Fpnl at order p from U (Taylor class)

% version 20/3/2017 
%
% Taylor coefficients of U until order p-1 are stored in Us
U1=get(U,'coef1');
Us=zeros(size(U1,1),p-1);
Us(:,1)=U1;
for r=2:p-1
 Us(:,r)= get(U,'coefk',r);
end

% convolution 
valQ=sum(Us(obj.jQ,:).*Us(obj.kQ,p-1:-1:1),2);
valdQ=sum(Us(obj.jdQ,:).*Us(obj.kdQ,p-1:-1:1).*(p-1:-1:1)/p,2);
                        
Fpnl_tot = sparse(obj.iQ,ones(1,size(obj.iQ,1)),obj.vQ.*valQ,obj.neq_tot,1) ...
    +  sparse(obj.idQ,ones(1,size(obj.idQ,1)),obj.vdQ.*valdQ,obj.neq_tot,1);

end