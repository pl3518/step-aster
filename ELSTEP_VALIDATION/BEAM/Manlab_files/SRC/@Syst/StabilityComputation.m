function [Uchange,Achange,status,eigen] = StabilityComputation(obj,U0,Amax,status_Uinit,nb_realpos)

if status_Uinit == 1
    status{1} = 'stable';
else
    status{1} = 'unstable';
end

nb_realpos1 = nb_realpos;
Umax = evalseries(U0,Amax,obj.order);
Jacobian_str = Stab_Jacobian(obj,Umax);
[flag2,nb_realpos2,eig_val,eig_vec] = obj.Stability(Umax,Jacobian_str);
    
    Umid = Umax;
    Amid = Amax-1e-12; % For the case of Amax being very close to the change of stability.
    
    if ((nb_realpos1 ~= nb_realpos2) && (sum(sum(isnan(eig_vec)) == 0)))
        disp('Change of the stability detected. Computation of the exact point.');
        A1 = 0;
        A2 = Amax;
        nb_iter = 0;
        
        % Tolerance on relative position of Achange, 1 percent :
        A_tol = sqrt(obj.StabTol);
        
        while ((A2-A1)/A2>A_tol && nb_realpos1~=nb_realpos2 && nb_iter < obj.NRitemax)
            
            Amid = (A2+A1)/2;
            Umid = evalseries(U0,Amid,obj.order);
            Jacobian_str=Stab_Jacobian(obj,Umid);
            
            [~,nb_realpos,eig_val,eig_vec] = obj.Stability(Umid,Jacobian_str);
            
            %        disp(num2str(eig_val)); % Debug
            
            if nb_realpos == nb_realpos1
                A1 = Amid;
                nb_realpos1 = nb_realpos;
            elseif nb_realpos == nb_realpos2
                A2 = Amid;
                nb_realpos2 = nb_realpos;
            else
                %disp('StabilityComputation : Error, Dichotomy did not converge.');
                nb_iter = obj.NRitemax;
            end
            nb_iter = nb_iter+1;
        end
        
        if flag2 == 1
            status{2} = 'stable';
        else
            status{2} = 'unstable';
        end
        
        Uchange = Umid;
        Achange = Amid;
        
        bif_type = 'nothing';
        
        switch obj.type
            case 'HBQ'
                floq_mult = exp(2*pi/Umid(obj.neq)*eig_val); % exp(T*rho)
                
                prod_m1 = abs(prod(floq_mult-1));
                prod_p1 = abs(prod(floq_mult+1));
                for i=1:numel(floq_mult)-1
                    bialt_prod(i) = prod( floq_mult(i)*floq_mult(i+1:end)-1);
                end
                min_abs_bialt_prod = min(abs(bialt_prod));
                
                if prod_m1 < min(prod_p1,min_abs_bialt_prod)
                    if abs(prod(floq_mult-1)) < A_tol
                        disp('Fold bifurcation / Branch point detected.');
                        bif_type = 'Fold';
                    end
                elseif prod_p1 < min_abs_bialt_prod
                    if abs(prod(floq_mult+1)) < A_tol
                        disp('Flip bifurcation detected.');
                        bif_type = 'PD';
                    end
                else
                    if min_abs_bialt_prod < A_tol
                        disp('Neimarck-Sacker bifurcation detected.');
                        bif_type = 'NS';
                    end
                end
            case 'AQ'
                
                product_eig = abs(prod(eig_val));
                for i=1:numel(eig_val)-1
                    prod_sum(i) = prod( eig_val(i) + eig_val(i+1:end));
                end
                min_abs_prodsum = min(abs(prod_sum));
                
                if product_eig < min_abs_prodsum
                    if product_eig < A_tol
                        disp('Pitchfork bifurcation detected');
                        bif_type = 'Pitchfork';
                    end
                else
                    if min_abs_prodsum < A_tol
                        disp('Hopf bifurcation detected.');
                        bif_type = 'Hopf';
                    end
                end
        end
        
        switch bif_type
            case {'Hopf','NS'}
                if abs(nb_realpos1-nb_realpos2) > 2
                    disp('StabilityComputation : There may be more than one bifurcation here.');
                end
            otherwise
                if abs(nb_realpos1-nb_realpos2) > 1
                    disp('StabilityComputation : There may be more than one bifurcation here.');
                end
        end
        
        eigen.type = bif_type;
        
    else
        eigen.type = 'nothing';
        Uchange = 0;
        Achange = 0;
        if status_Uinit == 1
            status{2} = 'stable';
        else
            status{2} = 'unstable';
        end
    end

eigen.values = eig_val;
eigen.vectors = eig_vec;

end

function [Jacobian_str] = Stab_Jacobian(obj,U)
Jacobian_str = obj.Jacobian(U);
[Jacobian_str.LK_aux,Jacobian_str.UK_aux] = lu(Jacobian_str.dRauxdUaux,'vector');
Kfull = Jacobian_str.dRdU - Jacobian_str.dRdUaux*(Jacobian_str.UK_aux\(Jacobian_str.LK_aux\Jacobian_str.dRauxdU));
Jacobian_str.K = Kfull(:,1:end-1);
end



