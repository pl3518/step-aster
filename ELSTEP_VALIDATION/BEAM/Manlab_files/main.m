global U Section Diagram    % Global variables to export point from the diagram. 

addpath('SRC')  % Manlab path

% Parameters of the system: 
parameters=import_results_from_code_aster()


% Manlab variables
L=parameters.n_modes;
nz= 2*L;     % number of main equations of the DAE system
nz_aux =L*(L+1)/2;
%
H =3;     % number of harmonics




%% initialization of the system
sys=SystHBQ(nz,nz_aux,H,@equations,@point_display,@global_display,parameters,'forced');

% starting point
omega=850;lambda=omega;
%
Z0  =randn(2*H+1,sys.nz_tot)*1e-7;
%  x0 = Z0(:,1:sys.nz/2);
x0 = parameters.stiffness\parameters.force;
Z0(2,1:sys.nz/2)=x0;
%  y0 = Z0(:,sys.nz/2+1:sys.nz);
Z0(:,sys.nz/2+1:sys.nz) = omega*sys.D(Z0(:,1:sys.nz/2));      % y = x' in the Fourier domain.
%  r0 = Z0(:,sys.nz+1:sys.nz+nz_aux_geom);
r0 = zeros(sys.nz_aux,1);
for j=1:L
    r0((2*L-j+2)*(j-1)/2+1:(2*L-j+1)*j/2) = x0(j)*x0(j:L);
end
Z0(2,sys.nz+1:sys.nz+sys.nz_aux)=r0;

U0tot = sys.init_U0(Z0,omega,lambda);

% The bifurcation diagram represents
% first cosine of x with respect to lambda and
% first sine of x with respect to lambda :
dispvars = [sys.neq+1 2; ...
    sys.neq+1 2+H;
    sys.neq+1 1];

%% Launch of Manlab with options
Manlab('sys'       ,sys , ...
    'U0value'         ,U0tot, ...
    'ANMthreshold'    ,1e-7 , ...
    'NRthreshold'     ,1e-7, ...
    'StabilityCheck'  ,0, ...
    'displayvariables',dispvars);     % MANLAB run

