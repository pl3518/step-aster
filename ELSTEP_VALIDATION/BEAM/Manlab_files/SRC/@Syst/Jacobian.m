function Jacobian = Jacobian(obj,U)
% @SYS\Jacobian: Compute the Jacobian of R with respect to U

ninc = obj.ninc ;
dRdU = sparse(obj.neq,0);
global Ck;
Ck=1;
Uw=Taylor(get(obj,'order'),U);   % Working vector (Taylor type) 
  
for n=1:ninc
    %choice of a pertubation vector in the canonical basis  
    Ue = zeros(ninc,1); Ue(n) = 1; Uw = set(Uw,'coef1',Ue);
    %computation of the Jacobian in this direction, result in the Taylor
    %coefficient at order 1
    Re = get(R(obj,Uw),'coef1');
    dRdU = [dRdU, sparse(Re)];
end

Jacobian.dRtotdUtot = dRdU;
Jacobian.dRdU = dRdU;
Jacobian.dRauxdUaux=[];

Ck=obj.order;