function [Utot] = NRcorrection(obj,Utot)
% performs Newton-Raphson correction

iter = 0;

NRthreshold=get(obj,'NRthreshold'); NRitemax=get(obj,'NRitemax');
Usave = Utot;
Rnew_tot=obj.R(obj,Utot) + obj.pertvect*obj.PerturbationSize;
normR =norm(Rnew_tot);
best_normR = normR;
if (normR > NRthreshold)
    disp(['N-R: Before correction ||R(Uj)||=' num2str(normR); ] );
end

while (normR > NRthreshold) && (iter < NRitemax)
    iter = iter + 1;
    Jacobian = obj.Jacobian(Utot);
    
    % Check if Utot is a limit-point and update the structure Jacobian.
    [~,~,Jacobian] = obj.tangentvector(Jacobian);
    
    % Corrections thanks to the condensation function
    [Ucor,Ucor_aux] = obj.Condensation(Jacobian,Rnew_tot(1:obj.neq),Rnew_tot(obj.neq+1:end),0);
    Ucor_tot = [Ucor;Ucor_aux];
    
    Utot=Utot-Ucor_tot;
    
    Rnew_tot=obj.R(obj,Utot) + obj.pertvect*obj.PerturbationSize;
    normR = norm(Rnew_tot);
    if normR < best_normR
        best_normR = normR;
        Usave = Utot;
    end
    disp(['N-R: After correction ' num2str(iter) ' : ||R(Uj)||=' num2str(normR); ] );
end

if isnan(normR)
    warndlg('Correction failed: NaN values. Back to the best point.','Correction','on');
    Utot = Usave;
end

if iter == NRitemax
    disp('Correction failed: Max number of iterations is reached. Back to the best point.');
    Utot = Usave;
end
