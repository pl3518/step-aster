function [Fpnl_tot] = Fpnl(obj,U,p)
% Compute  the r.h.s Fpnl at order p from U (Taylor class)

H    = obj.H;
nz_tot   = obj.nz_tot;

DHp1=2*H+1 ;

Fpnl_fourier=zeros(DHp1,nz_tot);
aux_var = zeros(2,1);
cond_ini = zeros(1,nz_tot);
for r=1:p-1
    Ur  =get(U,'coefk',r) ;
    [Zr_tot,omegar,lambdar,omega2r,lambdaomegar] = obj.get_Ztot(Ur);
    Upmr=get(U,'coefk',p-r);
    [Zpmr_tot,omegapmr] = obj.get_Ztot(Upmr);
    
    Zr_t0 = sum(Zr_tot(1:H+1,:),1);
    Zpmr_t0 = sum(Zpmr_tot(1:H+1,:),1);
    
    %boucle sur les listes
    for i=1:numel(obj.ic2)
        Fpnl_fourier(1,obj.ic2(i))= Fpnl_fourier(1,obj.ic2(i)) + obj.vc2(i)*lambdar*lambdapmr ;
    end
    
    for i=1:numel(obj.il1)
        Fpnl_fourier(:,obj.il1(i))= Fpnl_fourier(:,obj.il1(i)) + (obj.vl1(i)*lambdar)*Zpmr_tot(:,obj.jl1(i)) ;
    end
    
    for i=1:numel(obj.iq)
        Fpnl_fourier(:,obj.iq(i))= Fpnl_fourier(:,obj.iq(i)) + obj.vq(i)*( obj.Prod(Zr_tot(:,obj.jq(i)),Zpmr_tot(:,obj.kq(i))) ) ;
    end
    
    for i=1:numel(obj.id)
        Fpnl_fourier(:,obj.id(i))= Fpnl_fourier(:,obj.id(i)) + (obj.vd(i)*omegar)*obj.D(Zpmr_tot(:,obj.jd(i))) ;
    end
    
    for i=1:numel(obj.idd)
        Fpnl_fourier(:,obj.idd(i))= Fpnl_fourier(:,obj.idd(i)) + (obj.vdd(i)*omega2r)*obj.D(obj.D(Zpmr_tot(:,obj.jdd(i)))) ;
    end
    
    for i=1:numel(obj.id1)
        Fpnl_fourier(:,obj.id1(i))= Fpnl_fourier(:,obj.id1(i)) + (obj.vd1(i)*lambdaomegar)*obj.D(Zpmr_tot(:,obj.jd1(i))) ;
    end
    
    % definition of omega2 and lambdaomega :
    aux_var = aux_var - [omegar*omegapmr; lambdar*omegapmr];
    
    if ~isempty(obj.idl)
        for i=1:numel(obj.idq)
            Fpnl_fourier(:,obj.idq(i))= Fpnl_fourier(:,obj.idq(i)) + obj.vdq(i)*( obj.Prod(Zr_tot(:,obj.jdq(i)),obj.D(Zpmr_tot(:,obj.kdq(i))))) ;
        end
        
        cond_ini(obj.idq)= cond_ini(obj.idq) + (p-r)/p*obj.vdq'.*Zr_t0(obj.jdq).*Zpmr_t0(obj.kdq) ;
    end
end

% transform the matrix Fpnl to a column vector and add 0 for the phase
% equation, omega2,.. eqautions
Fpnl_fourier(1,obj.idq) = cond_ini(obj.idq);
Fpnl_fourier = reshape(Fpnl_fourier,nz_tot*DHp1,1);

Fpnl = [Fpnl_fourier(1:obj.neq-1);0];
Fpnl_aux = [Fpnl_fourier(obj.neq:end);aux_var];
Fpnl_tot = [Fpnl;Fpnl_aux];

