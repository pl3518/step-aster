function [] = global_display(obj,Section)
% Section is a 'CheckPoint class' object 
% containing all the information about the series:
% discrete representation, bifurcation analysis,...

Upp = Section.Upp; % Point representation of the series
%Floquet_exponent = Section.Eigen.values;
 
H=obj.H;
nz_tot=obj.nz_tot;
tol_h = 10*obj.NRthreshold;

nb_pt = size(Upp,2);

h = zeros(nb_pt,nz_tot);

%% Loop on the point representation of the series
for k = 1:nb_pt
    
    Uk = Upp(:,k);
    [Zk,omega(k),lambda(k)] = obj.get_Ztot(Uk);
    
    T=2*pi/omega(k);
    temps=0:T/(40*H+1):T;
    nt=length(temps);

    VectCos=cos(omega(k)*temps'*(1:H));
    VectSin=sin(omega(k)*temps'*(1:H));
    MAT=[ ones(nt,1) , VectCos , VectSin ];  % MAT(nt,2*H+1)
    Ut=MAT*Zk;  % Ut(nt,nz)
    
    % 29 nodes along the axis
    %Displx=Ut(:,1:obj.nz/2)*obj.parameters.Phi(obj.parameters.axis_nodes*3-2,:)';  %Displx(nt,29)
    Disply=Ut(:,1:obj.nz/2)*obj.parameters.Phi(obj.parameters.axis_nodes*3-1,:)';
    Displz=Ut(:,1:obj.nz/2)*obj.parameters.Phi(obj.parameters.axis_nodes*3,:)';
    % 15 is the center node, 13 is the node at .433m
    center=15;
    decentered=13;
    
    AF1_c=(max(Disply(:,center))-min(Disply(:,center)))/2;
    %
    AF1_d=(max(Disply(:,decentered))-min(Disply(:,decentered)))/2;
    %
    AL4_d=(max(Displz(:,decentered))-min(Displz(:,decentered)))/2;
    
    
    figure(200);hold on; 
    title('y-displacement amplitude of center node');     
    plot(omega(k)/2/pi,AF1_c,'b.');
    
    figure(300);hold on; 
    title('y-displacement amplitude of node at z=0.433m');     
    plot(omega(k)/2/pi,AF1_d,'b.');
    
    figure(400);hold on; 
    title('z-displacement amplitude of node at z=0.433m');        
    plot(omega(k)/2/pi,AL4_d,'k.');

    

    
%     for i=1:nz_tot
%         norm_Z = norm(Zk(:,i));
%         nono_Z = norm(Zk(1,i));
%         while (nono_Z/norm_Z< 1-tol_h)
%             h(k,i) = h(k,i)+1;
%             ind = [ (1:h(k,i)+1) (H+2:H+1+h(k,i)) ];
%             nono_Z = norm(Zk(ind,i));
%         end
%     end

end

% figure(5);
% % Amplitud ratios
% for i=1:nz_tot
%     if i>obj.nz; color = 'b'; else; color = 'r'; end
%     pl(i)=plot(lambda,h(:,i),[color '-*']);hold on;grid on;
% end
% format short e
% title(['Number of harmonics needed to reach the Fourier norm with ' num2str(tol_h) ' precision.'])
% legend([pl(1) pl(end)],'Main variables','Auxiliary variables','Location','SouthEast');

end
