function [obj] = get_operators(obj)
%function [sys] = get_operators(obj)
%This function compute the operator used by Manlab from the quadratic
%recast of the equations.

neq_tot = obj.neq_tot;
neq = obj.neq;
neq_aux = obj.neq_aux;
ninc = obj.ninc;
equations = obj.equations;
eps = 1e-14;

    function [L_Qsym,dL_dQ] = eq_bilin(sys,U,V,varargin)
        if nargin == 3
            [Rout1] = equations(sys,V+U);
            [Rout2] = equations(sys,V-U);
            L_Qsym = (Rout1-Rout2)/2;
        else
            [Rout1,dRout1] = equations(sys,V+U,varargin{:});
            [Rout2,dRout2] = equations(sys,V-U,varargin{:});
            L_Qsym = (Rout1-Rout2)/2;
            dL_dQ = (dRout1-dRout2)/2;
        end
 
        return
    end

obj.iC = []; obj.vC = [];
obj.iL = []; obj.jL = []; obj.vL = [];
obj.iQ = []; obj.jQ=[]; obj.kQ=[]; obj.vQ=[];

Z = sparse(ninc,1);
Id = speye(ninc);

valL = zeros(neq_tot,ninc);
fun_eq_bilin = @eq_bilin;

vQ = cell(ninc,1);
iQ = cell(ninc,1);
jQ = cell(ninc,1);
kQ = cell(ninc,1);

if nargin(equations) == 2
    
    %% constant operator
    valc = equations(obj,Z);
    
    %% linear and quadratic operators
    
    %%% Parallelization of the construction for more efficiency.
    parfor j=1:ninc
        
        valL(:,j) = feval(fun_eq_bilin,obj,Id(:,j),Z);
        
        valQ = zeros(neq_tot,ninc)
        for k=1:ninc
            valQ(:,k) = feval(fun_eq_bilin,obj,Id(:,j),Id(:,k))
        end
        valQ = (valQ - valL(:,j))/2;
        valQ(abs(valQ)<eps)=0;
        [ind_iQ,ind_kQ,ind_vQ] = find(valQ);
        vQ{j} = ind_vQ;
        iQ{j} = ind_iQ;
        jQ{j} = j*ones(size(ind_iQ));
        kQ{j} = ind_kQ;
    end
    
elseif nargin(equations) == 3
    
    [~,dRtest] = equations(obj,randn(ninc,1),randn(ninc,1));
    ind_diff = find(dRtest) + neq;

    obj.idL = []; obj.jdL = []; obj.vdL = [];
    obj.idQ = []; obj.jdQ=[]; obj.kdQ=[]; obj.vdQ=[];
    
    %% constant operator
    valc = equations(obj,Z,Z);
        valc(ind_diff) = 0;

    %% linear and quadratic operators
    vdQ = cell(ninc,1);
    idQ = cell(ninc,1);
    jdQ = cell(ninc,1);
    kdQ = cell(ninc,1);
    
    valdL = zeros(neq_aux,ninc);
    %%% Parallelization of the construction for more efficiency.
    parfor j=1:ninc
        
        valL(:,j) = feval(fun_eq_bilin,obj,Id(:,j),Z,Z);
        [~,valdL(:,j)] = feval(equations,obj,Z,Id(:,j));
        
        valQ = zeros(neq_tot,ninc);
        valdQ = zeros(neq_aux,ninc);
        for k=1:ninc
            valQ(:,k) = feval(fun_eq_bilin,obj,Id(:,j),Id(:,k),Z);
            [~,valdQ(:,k)] = feval(equations,obj,Id(:,k),Id(:,j));
        end
        valQ = (valQ - valL(:,j))/2;
        valQ(ind_diff,:) = 0;
        valQ(abs(valQ)<eps)=0;
        [ind_iQ,ind_kQ,ind_vQ] = find(valQ);
        vQ{j} = ind_vQ;
        iQ{j} = ind_iQ;
        jQ{j} = j*ones(size(ind_iQ));
        kQ{j} = ind_kQ;
        
        valdQ = (valdQ - valdL(:,j));
        valdQ(abs(valdQ)<eps)=0;
        [ind_idQ,ind_kdQ,ind_vdQ] = find(valdQ);
        vdQ{j} = ind_vdQ;
        idQ{j} = ind_idQ;
        kdQ{j} = j*ones(size(ind_idQ));
        jdQ{j} = ind_kdQ;
    end
    
    valdL(abs(valdL)<eps)=0;
    [obj.idL,obj.jdL,obj.vdL] = find(valdL);
    
    obj.vdQ = cell2mat(vdQ);
    obj.idQ = cell2mat(idQ);
    obj.jdQ = cell2mat(jdQ);
    obj.kdQ = cell2mat(kdQ);
    
    % Indices augmented by neq : only Raux can be affected by differentiation.
    obj.idL = obj.idL + obj.neq;
    obj.idQ = obj.idQ + obj.neq;
    
    valL(ind_diff,:) = 0;
end

%% constant operator
valc(abs(valc)<eps)=0;
[obj.iC,~,obj.vC] = find(valc);

%% linear operator
valL(abs(valL)<eps)=0;
[obj.iL,obj.jL,obj.vL] = find(valL);

%% quadratic operator
obj.vQ = cell2mat(vQ);
obj.iQ = cell2mat(iQ);
obj.jQ = cell2mat(jQ);
obj.kQ = cell2mat(kQ);
end