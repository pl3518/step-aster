function Jacobian = Jacobian(obj,Utot)
% Compute the Jacobian of R with respect to U

H    = obj.H;
nz_tot   = obj.nz_tot;

DHp1=2*H+1 ;
DHp12=DHp1*DHp1;


% Extract informations from Utot
[Ztot,omega,lambda,omega2,lambdaomega] = obj.get_Ztot(Utot);

Z_t0 = sum(Ztot(1:H+1,:),1);

% definition of the small (constant)matrix : I and d
%
% Identity matrix (2*H+1) sparse
[iI,jI,vI]=find(speye(DHp1)) ;

% d sparse matrix
idmat=[ 1 ,   2:H+1  ,H+2:DHp1 ]';
jdmat=[ 1 , H+2:DHp1 ,  2:H+1  ]';
vdmat=[ 0 ,     1:H  , -(1:H)  ]';

% dd sparse matrix
iddmat=[ 1 ,   2:H+1  ,H+2:DHp1 ]';
jddmat=iddmat;
vddmat=-[ 0 ,     (1:H).^2  , (1:H).^2  ]';

% ib and jb for b sparce matrix
[ib,jb,~]=find( sparse( ones(DHp1,DHp1) ));

%   dRdZ =  L0 + lambda L1  + 2*Q(Z,.)
%   construct the list for the large matrix L0, L1, dl, 2*Q(Z,.) and dq(Z,.)
nl0=numel(obj.il0); iL0=zeros(DHp1,nl0); jL0=zeros(DHp1,nl0); vL0=zeros(DHp1,nl0);
for i=1:nl0
    iL0(:,i)=iI + (obj.il0(i)-1)*DHp1 ;
    jL0(:,i)=jI + (obj.jl0(i)-1)*DHp1 ;
    vL0(:,i)=vI*obj.vl0(i)  ;
end

nl1=numel(obj.il1); iL1=zeros(DHp1,nl1); jL1=zeros(DHp1,nl1); vL1=zeros(DHp1,nl1);
for i=1:nl1
    iL1(:,i)=iI + (obj.il1(i)-1)*DHp1 ;
    jL1(:,i)=jI + (obj.jl1(i)-1)*DHp1 ;
    vL1(:,i)=vI*(obj.vl1(i)*lambda) ;
end

nq=numel(obj.iq); iQL=zeros(DHp12,nq); jQL=zeros(DHp12,nq); vQL=zeros(DHp12,nq);
for i=1:nq
    % Avoid to compute the same B several times / it uses that obj.jq is non-decreasing.
    if ~(i>1 && obj.jq(i-1) == obj.jq(i))
        vb= reshape(obj.B(Ztot(:,obj.jq(i))) ,DHp12,1) ;
    end
    iQL(:,i)=ib + (obj.iq(i)-1)*DHp1 ;
    jQL(:,i)=jb + (obj.kq(i)-1)*DHp1 ;
    vQL(:,i)=2*vb*obj.vq(i) ;
end

nld=numel(obj.id); iD=zeros(DHp1,nld); jD=zeros(DHp1,nld); vD=zeros(DHp1,nld);
for i=1:nld
    iD(:,i)=idmat + (obj.id(i)-1)*DHp1 ;
    jD(:,i)=jdmat + (obj.jd(i)-1)*DHp1 ;
    vD(:,i)=vdmat*(obj.vd(i)*omega) ;
end

nldd=numel(obj.idd); idd=zeros(DHp1,nldd); jdd=zeros(DHp1,nldd); vdd=zeros(DHp1,nldd);
for i=1:nldd
    idd(:,i)=iddmat + (obj.idd(i)-1)*DHp1 ;
    jdd(:,i)=jddmat + (obj.jdd(i)-1)*DHp1 ;
    vdd(:,i)=vddmat*(obj.vdd(i)*omega2) ;
end

ndl=numel(obj.idl); idl=zeros(DHp1,ndl); jdl=zeros(DHp1,ndl); vdl=zeros(DHp1,ndl);
for i=1:ndl
    idl(:,i)=idmat + (obj.idl(i)-1)*DHp1 ;
    jdl(:,i)=jdmat + (obj.jdl(i)-1)*DHp1 ;
    vdl(:,i)=vdmat*obj.vdl(i) ;
end

idq1mat = repmat(idmat,DHp1,1);
jdq1mat = reshape(repmat(jdmat',DHp1,1),DHp12,1);
vdq1mat = reshape(repmat(vdmat',DHp1,1),DHp12,1);
ndq=numel(obj.idq); idq1=zeros(DHp12,ndq); jdq1=zeros(DHp12,ndq); vdq1=zeros(DHp12,ndq);
for i=1:ndq
    vb= reshape(obj.B(Ztot(:,obj.jdq(i))),DHp12,1) ;
    idq1(:,i)=idq1mat + (obj.idq(i)-1)*DHp1 ;
    jdq1(:,i)=jdq1mat + (obj.kdq(i)-1)*DHp1 ;
    vdq1(:,i)=(vdq1mat.*vb)*obj.vdq(i) ;
end

idq2=zeros(DHp12,ndq); jdq2=zeros(DHp12,ndq); vdq2=zeros(DHp12,ndq);
for i=1:ndq
    vb= reshape(obj.B(obj.D(Ztot(:,obj.kdq(i)))),DHp12,1) ;
    idq2(:,i)=ib + (obj.idq(i)-1)*DHp1 ;
    jdq2(:,i)=jb + (obj.jdq(i)-1)*DHp1 ;
    vdq2(:,i)=vb*obj.vdq(i) ;
end

nld1=numel(obj.id1); id1=zeros(DHp1,nld1); jd1=zeros(DHp1,nld1); vd1=zeros(DHp1,nld1);
for i=1:nld1
    id1(:,i)=idmat + (obj.id1(i)-1)*DHp1 ;
    jd1(:,i)=jdmat + (obj.jd1(i)-1)*DHp1 ;
    vd1(:,i)=vdmat*(obj.vd1(i)*lambdaomega) ;
end

idRdZ=[ reshape(iL0,nl0*DHp1,1) ; reshape(iL1,nl1*DHp1,1) ; ...
    reshape(iQL,nq*DHp12,1) ; reshape(iD,nld*DHp1,1); ...
    reshape(id1,nld1*DHp1,1) ; reshape(idd,nldd*DHp1,1); ...
    reshape(idq2,ndq*DHp12,1) ; reshape(idq1,ndq*DHp12,1) ; reshape(idl,ndl*(DHp1),1)];

jdRdZ=[ reshape(jL0,nl0*DHp1,1) ; reshape(jL1,nl1*DHp1,1) ; ...
    reshape(jQL,nq*DHp12,1) ; reshape(jD,nld*DHp1,1); ...
    reshape(jd1,nld1*DHp1,1) ; reshape(jdd,nldd*DHp1,1); ...
    reshape(jdq2,ndq*DHp12,1) ; reshape(jdq1,ndq*DHp12,1) ; reshape(jdl,ndl*(DHp1),1)];

vdRdZ=[ reshape(vL0,nl0*DHp1,1) ; reshape(vL1,nl1*DHp1,1) ; ...
    reshape(vQL,nq*DHp12,1) ; reshape(vD,nld*DHp1,1); ...
    reshape(vd1,nld1*DHp1,1) ; reshape(vdd,nldd*DHp1,1); ...
    reshape(vdq2,ndq*DHp12,1) ; reshape(vdq1,ndq*DHp12,1) ; reshape(vdl,ndl*(DHp1),1)];

dRdZ=sparse(idRdZ,jdRdZ,vdRdZ,DHp1*nz_tot,DHp1*nz_tot) ;

%   Modification for dl/dq : initial condition replace the zero-th harmonic :
dRdZ(DHp1*(obj.idl-1)+1,:) = 0;
for i=1:numel(obj.idl)
    dRdZ(DHp1*(obj.idl(i)-1)+1,DHp1*(obj.jdl(i)-1)+(1:H+1)) = obj.vdl(i);
end
for i=1:numel(obj.idq)
    dRdZ(DHp1*(obj.idq(i)-1)+1,DHp1*(obj.kdq(i)-1)+(1:H+1)) = obj.vdq(i)*Z_t0(obj.jdq(i))  +  dRdZ(DHp1*(obj.idq(i)-1)+1,DHp1*(obj.kdq(i)-1)+(1:H+1));
end

%   dRdomega  = D(Z)
dRdomega =zeros(DHp1,nz_tot);
for i=1:nld
    dRdomega(:,obj.id(i))= dRdomega(:,obj.id(i)) + obj.vd(i)*obj.D(Ztot(:,obj.jd(i))) ;
end
dRdomega=reshape(dRdomega,DHp1*nz_tot,1);

%   dRdomega2  = dd(Z)
dRdomega2 =zeros(DHp1,nz_tot);
for i=1:nldd
    dRdomega2(:,obj.idd(i))= dRdomega2(:,obj.idd(i)) + obj.vdd(i)*obj.D(obj.D(Ztot(:,obj.jdd(i)))) ;
end
dRdomega2=reshape(dRdomega2,DHp1*nz_tot,1);
% equation defining omega2
rowomega2 = sparse([1 1],[obj.neq obj.ninc-1],[-2*omega 1],1,obj.ninc);

%   dRd1ambda  = C1 + L1(Z) + 2*lambda*C2
dRdlambda =zeros(DHp1,nz_tot);
for i=1:numel(obj.iforce1)
    dRdlambda(obj.hforce1(i),obj.iforce1(i)) = dRdlambda(obj.hforce1(i),obj.iforce1(i)) - obj.vforce1(i);
end
for i=1:numel(obj.ic1)
    dRdlambda(1,obj.ic1(i))= dRdlambda(1,obj.ic1(i)) + obj.vc1(i) ;
end
for i=1:numel(obj.ic2)
    dRdlambda(1,obj.ic2(i))= dRdlambda(1,obj.ic2(i)) + obj.vc2(i)*2*lambda ;
end
for i=1:numel(obj.il1)
    dRdlambda(:,obj.il1(i))= dRdlambda(:,obj.il1(i)) + obj.vl1(i)*Ztot(:,obj.jl1(i)) ;
end
dRdlambda=reshape(dRdlambda,DHp1*nz_tot,1);

%   dRd1ambdaomega  = d1(Z)
dRdlambdaomega =zeros(DHp1,nz_tot);
for i=1:numel(obj.id1)
    dRdlambdaomega(:,obj.id1(i))= dRdlambdaomega(:,obj.id1(i)) + obj.vd1(i)*obj.D(Ztot(:,obj.jd1(i))) ;
end
dRdlambdaomega=reshape(dRdlambdaomega,DHp1*nz_tot,1);
% Equation defining lambdaomega
rowlambdaomega = sparse([1 1 1],[obj.neq obj.neq+1 obj.ninc],[-lambda -omega 1],1,obj.ninc);

% phase equation zi'(0)=0 -> zis1 + 2*zis2 + 3*zis3 + ...  =0
switch obj.subtype
    case 'autonomous'
        if obj.zi_phase <= obj.nz; indice=(obj.zi_phase-1)*DHp1; else, indice=(obj.zi_phase-1)*DHp1+2; end
        rowphase=sparse( ones(1,H) ,indice+H+2:indice+DHp1, [1:H],1,obj.ninc) ;
        
    case 'forced'
        if isfloat(obj.pulsation) % omega fixed
            rowphase=sparse(1,obj.neq,1,1,obj.ninc);
        else % omega is the continuation parameter
            rowphase=sparse( ones(1,2) ,obj.neq:obj.neq+1,[1 -1],1,obj.ninc);
        end
end

% Block-decomposition of the Jacobian matrix for the condensation of the
% auxiliar variables

Jacobian.dRdU = [dRdZ(1:obj.neq-1,1:obj.neq-1),sparse(dRdomega(1:obj.neq-1)),sparse(dRdlambda(1:obj.neq-1)) ; rowphase(1:obj.neq+1)];

Jacobian.dRdUaux = [dRdZ(1:obj.neq-1,obj.neq:end), sparse(dRdomega2(1:obj.neq-1)) , sparse(dRdlambdaomega(1:obj.neq-1)) ; rowphase(obj.neq+2:end)];
Jacobian.dRauxdU = [dRdZ(obj.neq:end,1:obj.neq-1), sparse(dRdomega(obj.neq:end)) , sparse(dRdlambda(obj.neq:end)) ; rowomega2(1:obj.neq+1) ; rowlambdaomega(1:obj.neq+1)];
Jacobian.dRauxdUaux = [dRdZ(obj.neq:end,obj.neq:end), sparse(dRdomega2(obj.neq:end)) , sparse(dRdlambdaomega(obj.neq:end)) ; rowomega2(obj.neq+2:end) ; rowlambdaomega(obj.neq+2:end)];

Jacobian.dRtotdUtot = [[Jacobian.dRdU ; Jacobian.dRauxdU], [Jacobian.dRdUaux ; Jacobian.dRauxdUaux]];

end


